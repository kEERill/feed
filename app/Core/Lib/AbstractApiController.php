<?php
declare(strict_types=1);

namespace App\Core\Lib;

use App\Core\Lib\Interfaces\ModelAppInterface;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractApiController
{
    abstract protected function getEntityResource(): string;

    public function returnResource(ModelAppInterface $modelApp): Response
    {
        $className = $this->getEntityResource();
        /** @var BaseJsonResource $resource */
        $resource = new $className($modelApp);

        return $resource->response();
    }
}

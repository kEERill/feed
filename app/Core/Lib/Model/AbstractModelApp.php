<?php
declare(strict_types=1);

namespace App\Core\Lib\Model;

use App\Core\Lib\Interfaces\ModelAppInterface;
use Barryvdh\LaravelIdeHelper\Eloquent;
use DateTimeInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property DateTimeInterface $created_at
 * @property DateTimeInterface $updated_at
 *
 * @mixin Eloquent
 */
abstract class AbstractModelApp extends Model implements ModelAppInterface
{

}

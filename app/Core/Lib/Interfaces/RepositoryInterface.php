<?php
declare(strict_types=1);

namespace App\Core\Lib\Interfaces;

interface RepositoryInterface
{
    public function findById(int $id): ?ModelAppInterface;
}

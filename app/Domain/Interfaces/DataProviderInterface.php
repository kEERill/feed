<?php
declare(strict_types=1);

namespace App\Domain\Interfaces;

interface DataProviderInterface
{
    public function getNormalizedData(): NormalizedDataInterface;
}

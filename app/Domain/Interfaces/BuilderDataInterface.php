<?php
declare(strict_types=1);

namespace App\Domain\Interfaces;

interface BuilderDataInterface
{
    public function addDataProvider(DataProviderInterface $provider): self;
    
    public function compile(): void;
    
    public function getResult(): DenormalizedDataInterface;
}

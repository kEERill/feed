<?php
declare(strict_types=1);

namespace App\Domain\Feeds\Repositories;

use App\Core\Lib\Interfaces\RepositoryInterface;
use App\Domain\Feeds\Models\Feed;

class FeedsRepository implements RepositoryInterface
{
    public function findById(int $id): ?Feed
    {
        return Feed::query()->where(['id' => $id])->first();       
    }

}

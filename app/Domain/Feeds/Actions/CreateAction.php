<?php
declare(strict_types=1);

namespace App\Domain\Feeds\Actions;

use App\Domain\Feeds\Models\Feed;

class CreateAction
{
    public function execute(array $validatedData): Feed
    {
        $validatedData['created_by'] = $validatedData['user_id'];
        
        $entity = new Feed($validatedData);
        $entity->save();
        
        return $entity;
    }
}

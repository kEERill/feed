<?php
declare(strict_types=1);

namespace App\Domain\Feeds\Models;


use App\Core\Lib\Model\AbstractModelApp;

/**
 * @property string $name
 * @property string $type
 * @property int $created_by
 * @property int | null $updated_by
 */
class Feed extends AbstractModelApp
{
    public const TABLE_NAME = 'feeds';
    
    protected $table = self::TABLE_NAME;
    
    protected $fillable = [
        'created_by',
        'name',
        'type',
    ];
}

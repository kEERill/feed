<?php
declare(strict_types=1);

namespace App\Http\ApiV1\Modules\Feeds\Controllers;

use App\Core\Lib\AbstractApiController;
use App\Domain\Feeds\Actions\CreateAction;
use App\Domain\Feeds\Actions\PutAction;
use App\Domain\Feeds\Repositories\FeedsRepository;
use App\Http\ApiV1\Modules\Feeds\Requests\CreateFormRequest;
use App\Http\ApiV1\Modules\Feeds\Requests\PutFormRequest;
use App\Http\ApiV1\Modules\Feeds\Resources\FeedResource;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;

class FeedController extends AbstractApiController
{
    protected function getEntityResource(): string
    {
        return FeedResource::class;
    }

    public function create(CreateFormRequest $request, CreateAction $action): Response
    {
        return $this->returnResource($action->execute($request->validated()));
    }

    public function put(int $feedId, PutFormRequest $request, FeedsRepository $repo, PutAction $action): Response
    {
        if (null === $entity = $repo->findById($feedId)) {
            throw new ModelNotFoundException(sprintf('not FOUND!!!'));
        }

        return $this->returnResource($action->execute($entity, $request->validated()));
    }

    public function search()
    {
        //todo
    }

    public function delete()
    {
        //todo
    }

    public function get()
    {
        //todo
    }

}

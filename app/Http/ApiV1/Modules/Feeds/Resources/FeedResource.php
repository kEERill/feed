<?php
declare(strict_types=1);

namespace App\Http\ApiV1\Modules\Feeds\Resources;

use App\Domain\Feeds\Models\Feed;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @property Feed $resource;
 */
class FeedResource extends BaseJsonResource
{
    
    public function toArray($request)
    {
        return [
            'id'         => $this->resource->id,
            'name'       => $this->resource->name,
            'type'       => $this->resource->type,
            'created_by' => $this->resource->created_by,
            'updated_by' => $this->resource->updated_by,
            'created_at' => $this->resource->created_at,
            'updated_at' => $this->resource->updated_at,
        ];
    }

}

<?php
declare(strict_types=1);

namespace Database\Factories\Domain\Feeds\Models;

use App\Domain\Feeds\Models\Feed;
use Illuminate\Database\Eloquent\Factories\Factory;

class FeedFactory extends Factory
{
    protected $model = Feed::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name,
            'type' => 'some_type',

        ];
    }
}
